"use strict";

b4w.register("FrameProjectII", function (exports, require) {

    var m_app = require("app");
    var m_cam = require("camera");
    var m_ctl = require("controls");
    var m_data = require("data");
    var m_mouse = require("mouse");
    var m_obj = require("objects");
    var m_phy = require("physics");
    var m_scenes = require("scenes");
    var m_trans = require("transform");
    var m_util = require("util");
    var m_mat = require("material");
    var m_s_shoter = require("screenshooter");
    var m_quat = require("quat");
    // constants
    var OUTLINE_COLOR_VALID = [0, 1, 0];
    var OUTLINE_COLOR_ERROR = [1, 0, 0];
    var FLOOR_PLANE_NORMAL = [0, 1, 0];
    var WALL_X_MAX = 38;
    var WALL_X_MIN = -38;
    var WALL_Z_MAX = 38;
    var WALL_Z_MIN = -38;
    var ANGLE_90 = 90;
    var MENU_PANEL = "MenuPanel";
    var WARNING_PANEL = "WarningPanel";
    var INSTRUCTIONPANEL = "InstructionPanel";
    var ABOUT_PANEL = "AboutPanel";
    //Arrays
    var _obj_delta_xy = new Float32Array(2);
    var spawner_pos = new Float32Array(3);
    var _vec3_tmp = new Float32Array(3);
    var _vec3_tmp2 = new Float32Array(3);
    var _vec3_tmp3 = new Float32Array(3);
    var _vec4_tmp = new Float32Array(4);
    var frame_set_level_1 = new Array();
    var frame_set_level_2 = new Array();
    var buttons_set_color = new Array();
    var buttons_set_rotation = new Array();
    var buttons_set_size = new Array();
    var buttons_set_level = new Array();
    //frame property
    var p_f_level = 0;
    var p_f_color;
    var p_f_rotation;
    var p_f_rot_angle;
    var p_f_size_link;
    var p_f_size;
    var p_f_diff_intens;
    //other
    var _drag_mode = false;
    var _enable_camera_controls = true;
    var _selected_obj = null;
    //referens
    var active_color;
    var not_active_color;
    
    exports.init = function () {
        m_app.init({
            canvas_container_id: "main_canvas_container",
            callback: init_cb,
            physics_enabled: true,
            alpha: false,
            background_color: [0.8, 0.8, 0.8, 0.0]
        });
    };
    //registers canvas eventlistener and load main scene
    function init_cb(canvas_elem, success) {
        if (!success) {
            console.log("b4w init failure");
            return;
        }
        m_ctl.register_mouse_events(canvas_elem, true);
        m_ctl.register_wheel_events(canvas_elem, true);
        m_ctl.register_touch_events(canvas_elem, true);
        canvas_elem.addEventListener("mousedown", main_canvas_down);
        canvas_elem.addEventListener("touchstart", main_canvas_down);
        canvas_elem.addEventListener("mouseup", main_canvas_up);
        canvas_elem.addEventListener("touchend", main_canvas_up);
        canvas_elem.addEventListener("mousemove", main_canvas_move);
        canvas_elem.addEventListener("touchmove", main_canvas_move);
        window.onresize = m_app.resize_to_container;
        m_app.resize_to_container();
        m_data.load("FrameProgect.json", load_main_scene);
    }
    //load main scene
    function load_main_scene(data_id) {
        var spawner;
        m_app.enable_camera_controls();
        spawner = m_scenes.get_object_by_name("Spawner");
        m_trans.get_translation(spawner, spawner_pos);
        preload();
    }
    //load new frame scene
    function loaded_frame_scane(data_id) {
        var objs = m_scenes.get_all_objects("ALL", data_id);
        for (var i = 0; i < objs.length; i++) {
            construct_frame(objs[i]);
        }
    }

    function logic_func(s) {
        return s[1];
    }
    //change outline color according to the first collision sensor status
    function trigger_outline(obj, id, pulse) {
        var has_collision;
        if (pulse == 1) {
            has_collision = m_ctl.get_sensor_value(obj, id, 0);
            if (has_collision)
                m_scenes.set_outline_color(OUTLINE_COLOR_ERROR);
            else
                m_scenes.set_outline_color(OUTLINE_COLOR_VALID);
        }
    }
    //determines which object is selected
    function main_canvas_down(e) {
        var x, y, obj;
        if (e.preventDefault) { e.preventDefault(); }
        x = m_mouse.get_coords_x(e);
        var y = m_mouse.get_coords_y(e);
        var obj = m_scenes.pick_object(x, y);
        switch (m_scenes.get_object_name(obj)) {
            case "ButtonCreateFrame":
                if (checkFrameProp()) {
                    create_frame();
                }
                else {show_object_by_name(WARNING_PANEL); }
                return;
            case "UpLevelButton":
                change_level(obj, 1);
                return;
            case "DownLevelButton":
                change_level(obj, 2);
                return;
            case "WhiteButton":
                ChangeColorButtons(obj);
                return;
            case "BlackButton":
                ChangeColorButtons(obj);
                return;
            case "VengeButton":
                ChangeColorButtons(obj);
                return;
            case "RedButton":
                ChangeColorButtons(obj);
                return;
            case "VioletButton":
                ChangeColorButtons(obj);
                return;
            case "PurpleButton":
                ChangeColorButtons(obj);
                return;
            case "HorizontalButton":
                RotationButton(obj, ANGLE_90);
                return;
            case "VerticalButton":
                RotationButton(obj, 0);
                return;
            case "ScreenShoter":
                m_s_shoter.shot();
                return;
            case "Frame10x15Button":
                ChangeSizeButton(obj, "Resources/FrameWight10x15.json", 10);
                return;
            case "Frame15x20Button":
                ChangeSizeButton(obj, "Resources/FrameWight15x20.json", 15);
                return;
            case "Frame20x30Button":
                ChangeSizeButton(obj, "Resources/FrameWight20x30.json", 20);
                return;
            case "DeleteButton":
                DeleteFrame();
                return;
            case "ChColUpLevButton":
                ChangeColorFrameLvel(frame_set_level_2, p_f_color);
                return;
            case "ChColDownLevButton":
                ChangeColorFrameLvel(frame_set_level_1, p_f_color);
                return;
            case "ChColFrameButton":
                if (!_selected_obj) { return; }
                m_mat.set_diffuse_color(_selected_obj, m_mat.get_materials_names(_selected_obj)[0], p_f_color)
                return;
            case "ExiteWP":
                hide_object_by_name(WARNING_PANEL);
                return;
            case "ExiteMP":
                close_Menu_Panel();
                return;
            case "InformationB":
                hide_object_by_name(INSTRUCTIONPANEL);
                hide_object_by_name(ABOUT_PANEL);
                show_Menu_Panel(obj);
                return;
            case "InstructionB":
                close_Menu_Panel();
                show_object_by_name(INSTRUCTIONPANEL);
                return;
            case "ExiteIP":
                hide_object_by_name(INSTRUCTIONPANEL);
                return;
            case "ExiteAP":
                hide_object_by_name(ABOUT_PANEL);
                return;
            case "AboutB":
                close_Menu_Panel();
                show_object_by_name(ABOUT_PANEL);
                return;
            default:
                active_frame_outline_and_drag_eff(obj, x, y);
                break;
        }  
    }
    // enable camera controls after releasing the object
    function main_canvas_up(e) {
        _drag_mode = false;
        if (!_enable_camera_controls) {
            m_app.enable_camera_controls();
            _enable_camera_controls = true;
        }
    }
    //move frame
    function main_canvas_move(e) {
        var cam, x, y, camera_ray, cam_trans, point;
        if (!_drag_mode) { return; }
        if (!_selected_obj) { return; }
        if (_enable_camera_controls) {
            m_app.disable_camera_controls();
            _enable_camera_controls = false;
        }
        cam = m_scenes.get_active_camera();
        x = m_mouse.get_coords_x(e);
        y = m_mouse.get_coords_y(e);
        if (x >= 0 && y >= 0) {
            x -= _obj_delta_xy[0];
            y -= _obj_delta_xy[1];
            camera_ray = m_cam.calc_ray(cam, x, y, _vec3_tmp);
            cam_trans = m_trans.get_translation(cam, _vec3_tmp2);
            if (_selected_obj.level == 2) {
                point = m_util.line_plane_intersect(FLOOR_PLANE_NORMAL, 0, cam_trans, camera_ray, _vec3_tmp3);
            }
            else if (_selected_obj.level == 1) {
                point = m_util.line_plane_intersect(FLOOR_PLANE_NORMAL, -0.27, cam_trans, camera_ray, _vec3_tmp3);
            }
            if (point && camera_ray[1] < 0) {
                m_trans.set_translation_v(_selected_obj, point);
                limit_object_position(_selected_obj);
            }
        }
    }

    function create_frame() {
        m_data.load(p_f_size_link, loaded_frame_scane, null, null, true);
    }
    //limit move frame 
    function limit_object_position(obj) {
        var bb = m_trans.get_object_bounding_box(obj);
        var obj_parent = m_obj.get_parent(obj);
        var obj_pos = m_trans.get_translation(obj, _vec3_tmp);
        if (bb.max_x > WALL_X_MAX){
            obj_pos[0] -= bb.max_x - WALL_X_MAX;
        }      
        else if (bb.min_x < WALL_X_MIN) {
            obj_pos[0] += WALL_X_MIN - bb.min_x;
        }
        if (bb.max_z > WALL_Z_MAX){
            obj_pos[2] -= bb.max_z - WALL_Z_MAX;
        } 
        else if (bb.min_z < WALL_Z_MIN) {
            obj_pos[2] += WALL_Z_MIN - bb.min_z;
        }
        m_trans.set_translation_v(obj, obj_pos);
    }
    //check all properties on
    function checkFrameProp() {
        if (p_f_level == 0) { return false; }
        if (!p_f_color) { return false; }
        if (!p_f_rotation) { return false; }
        if (!p_f_size) { return false }
        return true;
    }
    //add frame to array
    function addToLevel(obj) {
        switch (p_f_level) {
            case 1:
                obj.level = 1;
                frame_set_level_1.push(obj);
                p_f_diff_intens = 0.9;
                break;
            case 2:
                obj.level = 2;
                frame_set_level_2.push(obj);
                p_f_diff_intens = 1;
                break;
        }
    }
    //set color for frame
    function ChangeColorButtons(obj) {
        p_f_color = m_mat.get_diffuse_color(obj, m_mat.get_materials_names(obj)[0]);
        toggle_button(obj, buttons_set_color, 1);
    }
    //set frame level
    function change_level(obj, level) {
        p_f_level = level;
        toggle_button(obj, buttons_set_level, 0);
    }
    //change rotation button
    function RotationButton(obj, angle) {
        p_f_rotation = m_trans.get_rotation(obj);
        p_f_rot_angle = angle;
        toggle_button(obj, buttons_set_rotation, 1);
    }
    //Change size button
    function ChangeSizeButton(obj, link, size) {
        p_f_size = size;
        p_f_size_link = link;
        toggle_button(obj, buttons_set_size, 0);
    }
    //Change color for frame level
    function ChangeColorFrameLvel(level_mass, color) {
        for (var i = 0; i < level_mass.length; i++) {
            m_mat.set_diffuse_color(level_mass[i], m_mat.get_materials_names(level_mass[i])[0], color);
        }
    }
    //Delet frame from scene
    function DeleteFrame() {
        var id;
        if (!_selected_obj) { return; }
        id = m_scenes.get_object_data_id(_selected_obj);
        m_data.unload(id);
        switch (_selected_obj.level){
            case 1:
                frame_set_level_1.splice(frame_set_level_1.indexOf(_selected_obj), 1);
                return;
            case 2:
                frame_set_level_2.splice(frame_set_level_2.indexOf(_selected_obj), 1);
                return;
        }
        _selected_obj = null;
    }
    //load all arrays and prepares main scene for work
    function preload() {
        buttons_set_color.push(m_scenes.get_object_by_name("WhiteButton"));
        buttons_set_color.push(m_scenes.get_object_by_name("BlackButton"));
        buttons_set_color.push(m_scenes.get_object_by_name("VengeButton"));
        buttons_set_color.push(m_scenes.get_object_by_name("RedButton"));
        buttons_set_color.push(m_scenes.get_object_by_name("VioletButton"));
        buttons_set_color.push(m_scenes.get_object_by_name("PurpleButton"));
        buttons_set_rotation.push(m_scenes.get_object_by_name("HorizontalButton"));
        buttons_set_rotation.push(m_scenes.get_object_by_name("VerticalButton"));
        buttons_set_size.push(m_scenes.get_object_by_name("Frame10x15Button"));
        buttons_set_size.push(m_scenes.get_object_by_name("Frame15x20Button"));
        buttons_set_size.push(m_scenes.get_object_by_name("Frame20x30Button"));
        buttons_set_level.push(m_scenes.get_object_by_name("UpLevelButton"));
        buttons_set_level.push(m_scenes.get_object_by_name("DownLevelButton"));
        hide_object_by_name(WARNING_PANEL);
        hide_object_by_name(MENU_PANEL);
        hide_object_by_name(ABOUT_PANEL);
        active_color = m_mat.get_diffuse_color(m_scenes.get_object_by_name("ColorSampleActive"), "CSActive");
        not_active_color = m_mat.get_diffuse_color(m_scenes.get_object_by_name("ColorSampleNotActive"), "CSNotActive");
    }
    //set rotation to frame befor it will be creat
    function setRotationToframe(obj) {
        var rot_obj_illust;
        if (p_f_rot_angle == ANGLE_90) {
            switch (p_f_size) {
                case 10:
                    rot_obj_illust = m_scenes.get_object_by_name("ColSam15x10");
                    break;
                case 15:
                    rot_obj_illust = m_scenes.get_object_by_name("ColSam20x15");
                    break;
                case 20:
                    rot_obj_illust = m_scenes.get_object_by_name("ColSam30x20");
                    break;
            }
            m_mat.inherit_material(rot_obj_illust, m_mat.get_materials_names(rot_obj_illust)[0], obj, m_mat.get_materials_names(obj)[1])
        }
        m_trans.set_rotation_v(obj, p_f_rotation);
    }
    //creat new frame
    function construct_frame(obj) {
        var sensor_col, sensor_sel;
        if (m_phy.has_physics(obj)) {
            m_phy.enable_simulation(obj);
            sensor_col = m_ctl.create_collision_sensor(obj, "Frame");
            sensor_sel = m_ctl.create_selection_sensor(obj);
            if (obj == _selected_obj) { m_ctl.set_custom_sensor(sensor_sel, 1); }
            m_ctl.create_sensor_manifold(obj, "COLLISION", m_ctl.CT_CONTINUOUS, [sensor_col, sensor_sel], logic_func, trigger_outline);
            m_trans.set_translation_v(obj, spawner_pos);
            addToLevel(obj);
            setRotationToframe(obj);
            m_mat.set_diffuse_color(obj, m_mat.get_materials_names(obj)[0], p_f_color)
            m_mat.set_diffuse_intensity(obj, m_mat.get_materials_names(obj)[0], p_f_diff_intens)
            //hide warrning panel
            m_scenes.hide_object(m_scenes.get_object_by_name(WARNING_PANEL));
        }
        if (m_obj.is_mesh(obj)) { m_scenes.show_object(obj); }
    }
    //active on frame outline and drag effects
    function active_frame_outline_and_drag_eff(obj, x, y) {
        var cam;
       _drag_mode = true;
        if (_selected_obj != obj) {
            if (_selected_obj) { m_scenes.clear_outline_anim(_selected_obj); }
            if (obj) { m_scenes.apply_outline_anim(obj, 1, 1, 0); }
            _selected_obj = obj;
        }
        if (_selected_obj) {
            cam = m_scenes.get_active_camera();
            m_trans.get_translation(_selected_obj, _vec3_tmp);
            m_cam.project_point(cam, _vec3_tmp, _obj_delta_xy);
            _obj_delta_xy[0] = x - _obj_delta_xy[0];
            _obj_delta_xy[1] = y - _obj_delta_xy[1];
        }
    } 
    //toggle button system
    function toggle_button(obj, array, number_button_material) {
        for (var i = 0; i < array.length; i++) {
            m_mat.set_diffuse_color(array[i], m_mat.get_materials_names(array[i])[number_button_material], not_active_color);
        }
        m_mat.set_diffuse_color(obj, m_mat.get_materials_names(obj)[number_button_material], active_color);
    }

    function show_Menu_Panel(obj) {
        m_mat.set_diffuse_color(obj, m_mat.get_materials_names(obj)[0], active_color);
        show_object_by_name(MENU_PANEL);
    }

    function close_Menu_Panel() {
        var information_button = m_scenes.get_object_by_name("InformationB")
        hide_object_by_name(MENU_PANEL) ;
        m_mat.set_diffuse_color(information_button, m_mat.get_materials_names(information_button)[0], not_active_color);
    }

    function show_object_by_name(obj_name) {
        m_scenes.show_object(m_scenes.get_object_by_name(obj_name));
    }

    function hide_object_by_name(obj_name) {
        m_scenes.hide_object(m_scenes.get_object_by_name(obj_name));
    }


});

b4w.require("FrameProjectII").init();